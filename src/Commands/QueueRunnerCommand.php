<?php

/**
 * @todo implement a real logger instead of passing around the output.
 */

declare(strict_types=1);

namespace Eicc\Fwq\QueueRunner\Commands;

use Eicc\Fwq\Exceptions\NoQueueSpecifiedException;
use Eicc\Fwq\Exceptions\QueueEmptyException;
use Eicc\Fwq\QueueRunner\Models\QueueRunner;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class QueueRunnerCommand extends Command
{
  protected $debug = false;
  protected ?string $queueName;
  protected ?string $workerName;
  protected int $count = -1;
  protected ?OutputInterface $output = null;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
         new InputOption('queue', '', InputOption::VALUE_REQUIRED, 'The queue to connect to'),
         new InputOption(
             'name',
             '',
             InputOption::VALUE_REQUIRED,
             "This runner's name. It must be unique amongst runners."
         ),
         new InputOption(
             'count',
             '',
             InputOption::VALUE_REQUIRED,
             "The number of jobs this runner works before exiting. "
         ),
        ];

    $this->setName('queue:runner')
        ->setDescription('Runs jobs pulled from a queue')
        ->setDefinition($definition)
        ->setHelp('Connects to the configured transport and queue then pops jobs off the queue and executes them.');
    return;
  }

  /**
   * Main body of this command
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $this->debug = $output->isDebug();
    $this->queueName = $input->getOption('queue') ?? '';
    $this->workerName = $input->getOption('name') ?? '';
    $this->count = (int)($input->getOption('count') ?? 100);
    $this->output->writeln('Featherweight Queue - QueueRunner');
    $this->output->writeln('  Queue Name    : ' . $this->queueName, OutputInterface::VERBOSITY_DEBUG);
    $this->output->writeln('  Worker Name   : ' . $this->workerName, OutputInterface::VERBOSITY_DEBUG);
    $this->output->writeln('  Max Job Count : ' . $this->count, OutputInterface::VERBOSITY_DEBUG);
    $this->output->writeln(' ', OutputInterface::VERBOSITY_DEBUG);


    if (empty($this->queueName)) {
      throw new NoQueueSpecifiedException();
    }
    $qr = new QueueRunner(
        $this->getApplication()->container,
        $this->queueName,
        $this->workerName,
        $this->count
    );
    try {
      $qr->mainLoop();
    } catch (QueueEmptyException $e) {
      // NO OP
    }

    $this->output->writeln('Done', OutputInterface::VERBOSITY_DEBUG) ;
    return Command::SUCCESS;
  }
}
