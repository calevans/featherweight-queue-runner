<?php

declare(strict_types=1);

namespace Eicc\Fwq\QueueRunner\Exceptions;

class MaxRetriesException extends \Exception
{
}
