<?php

declare(strict_types=1);

namespace Eicc\Fwq\QueueRunner\Models;

use Eicc\Fwq\Models\Job;
use Eicc\Fwq\Models\Queue;
use Eicc\Fwq\Exceptions\QueueEmptyException;
use Pimple\Container;

class QueueRunner
{
  protected $maxJobs = 100;
  protected $jobsLeft = 100;
  protected Container $container;
  protected ?string $queueName;
  protected ?string $WorkerName;
  protected ?Queue $queue = null;

  public function __construct(
      Container $container,
      string $queueName,
      string $workerName,
      int $maxJobs = 100
  ) {
    $this->container = $container;
    $this->maxJobs = $maxJobs;
    $this->jobsLeft = $maxJobs;
    $this->queueName = $queueName;
    $this->workerName = $workerName;
    $this->queue = $this->container['queue']($this->container,$this->queueName);
  }

  protected function executeJob(Job $job): void
  {
    $this->container['log']->debug("BEGIN executing job " . $job->getJobId());

    try {
      $job->execute();
    } catch (\Throwable $e) {
      $this->container['log']->error($e->getMessage());
    }
    $this->container['log']->debug("END executing job " . $job->getJobId());
  }

  public function mainLoop(): void
  {
    $this->container['log']->debug("Starting queue runner MAIN LOOP for " . $this->queueName);

    while ($this->continueLooping()) {

      $luw = $this->queue->pop($this->workerName);

      if (is_null($luw)) {
        $this->container['log']->debug($this->queueName . " is now empty. Exiting");
        throw new QueueEmptyException();
      }

      $this->executeJob(new Job($luw, $this->container));
      $this->container['log']->reset();
    }

    $this->container['log']->debug(
        "Reached max jobs of " . $this->maxJobs . " while working the queue " . $this->queueName . ". Exiting"
    );
  }

  protected function continueLooping(): bool
  {
    return ($this->maxJobs === -1) || ($this->jobsLeft-- > 0);
  }
}
