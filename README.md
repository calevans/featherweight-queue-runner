# Featherweight QueueRunner<br />
Cal Evans <cal@calevans.com>

This is the QueueRunner for the Featherweight Queueing System. This is not a standalone repo. You need to have featherweight-queu and a transport installed.

# Configuration Instructions

## As part of the main System
Nothing needs to be done, it will work as is.

## As a Standalone system
1. Install anad configure a Transport. Make sure it is the same transport you use in the main system. Each Transport's README contains configuration instructions.